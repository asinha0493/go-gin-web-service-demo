FROM golang:alpine

MAINTAINER Aniruddha Sinha

ENV GIN_MODE=release
ENV PORT=8080

WORKDIR /go/space

COPY web-service-gin /go/space/albums
#COPY templates /go/src/go-docker-dev.to/templates

# Run the two commands below to install git and dependencies for the project. 
RUN apk update && apk add --no-cache git
RUN go get ./...

#COPY dependencies /go/src #if you don't want to pull dependencies from git 

RUN go build /go/space/albums/

EXPOSE $PORT

ENTRYPOINT ["./web-service-gin"]